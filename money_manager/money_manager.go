package money_manager

import (
	"prikhodko.com/bee/hack/db"
	"prikhodko.com/bee/hack/contract_manager"
	"prikhodko.com/bee/hack/logger"
	"prikhodko.com/bee/hack/exchange"
	"math"
	"fmt"
	"prikhodko.com/bee/hack/common"
)

type (
	money struct {
		db     db.DB
		ctrmgr contract_manager.ContractManager

		pool []exchange.Exchange
	}
	Money interface {
		TakeMyMoney(login, password string, usd float64) error
		Change(login, base_cur, market_cur string, amount uint32, action common.Action) error
	}
)

func NewMoneyManager(db db.DB, ctr contract_manager.ContractManager) Money {
	_ = exchange.NewBittrexExchange()

	pool := make([]exchange.Exchange, 0)
	pool = append(pool, exchange.NewPoloniexExchange())
	pool = append(pool, exchange.NewBitfinexExchange())
	pool = append(pool, exchange.NewBittrexExchange())
	return &money{db, ctr, pool}
}

func (this *money) TakeMyMoney(login, password string, usd float64) error {
	contract, err := this.ctrmgr.GetContract(login)
	if err != nil {
		logger.Error("Cannot get contract %s", err.Error())
		return err
	}
	if err = this.ctrmgr.SendUsdMoney(contract, usd); err != nil {
		logger.Error("Cannot send money to contract %s", err.Error())
		return err
	}
	return nil
}

func (this *money) Change(login, base_cur, market_cur string, amount uint32, action common.Action) error {
	contract, err := this.ctrmgr.GetContract(login)
	if err != nil {
		return err
	}
	wallet_address_base, err := this.ctrmgr.GetWalletAddress(contract, base_cur)
	if err != nil {
		return err
	}
	wallet_address_market, err := this.ctrmgr.GetWalletAddress(contract, market_cur)
	if err != nil {
		return err
	}
	secret_base, err := this.db.GetSecret(login, contract, base_cur)
	if err != nil {
		return err
	}
	logger.Info("Manager is ready to change money addr=%s secret_base=%s wallet_address_market=%s",
		wallet_address_base, secret_base, wallet_address_market)

	var fin_ex exchange.Exchange
	var fin_price float64
	if action == common.ACTION_BUY {
		fin_price = math.MaxFloat64
	} else {
		fin_price = 0
	}
	for _, ex := range this.pool {
		bid, ask, err := ex.GetTicker(base_cur, market_cur)
		if err != nil {
			logger.Warn("Skip %s exchange. %s", ex.Type(), err.Error())
			continue
		}
		better, neww := common.Better(fin_price, bid, ask, action)
		if better {
			fin_price = neww
			fin_ex = ex
		}
		logger.Info("type= %s Bid %f Ask %f", ex.Type(), bid, ask)
	}
	if fin_ex == nil {
		return fmt.Errorf("not found any exchage for market = %s-%s", base_cur, market_cur)
	}
	logger.Info("The best exchange is %s price=%f", fin_ex.Type(), fin_price)
	return nil
}
