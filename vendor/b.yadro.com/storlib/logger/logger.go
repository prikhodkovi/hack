package logger

import (
	"fmt"

	"github.com/golang/glog"
)

const (
	logTrace = 2
	logDebug = 1
	logInfo  = 0
)

func format(args []interface{}) string {
	var resultString string = ""
	if len(args) > 0 {
		var format string
		format, ok := args[0].(string)
		if !ok {
			fmt, ok := args[0].(fmt.Stringer)
			if ok {
				format = fmt.String()
			}
		}
		newArgs := args[1:]
		resultString = fmt.Sprintf(format, newArgs...)
	}
	return resultString
}

// Info logs to the INFO log with v=0
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func Info(args ...interface{}) {
	if glog.V(logInfo) {
		glog.InfoDepth(1, format(args))
	}
}

// Debug logs to the INFO log with v=1
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func Debug(args ...interface{}) {
	if glog.V(logDebug) {
		glog.InfoDepth(1, format(args))
	}
}

// IsDebug indicates whether bedug logging is enabled
func IsDebug() bool {
	return bool(glog.V(logDebug))
}

// Trace logs to the INFO log with v=2
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func Trace(args ...interface{}) {
	if glog.V(logTrace) {
		glog.InfoDepth(1, format(args))
	}
}

// IsTrace indicates whether bedug logging is enabled
func IsTrace() bool {
	return bool(glog.V(logTrace))
}

// Warn logs to the WARNING and INFO logs.
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func Warn(args ...interface{}) {
	glog.WarningDepth(1, format(args))
}

// Error logs to the ERROR, WARNING, and INFO logs.
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func Error(args ...interface{}) {
	glog.ErrorDepth(1, format(args))
}

// ErrorDepth logs to the ERROR, WARNING, and INFO logs.
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func ErrorDepth(depth int, args ...interface{}) {
	glog.ErrorDepth(depth, format(args))
}

// Fatal logs to the FATAL, ERROR, WARNING, and INFO logs,
// including a stack trace of all running goroutines, then calls os.Exit(255).
// Arguments are handled in the manner of fmt.Print; a newline is appended if missing.
func Fatal(args ...interface{}) {
	glog.FatalDepth(1, format(args))
}
