package contract_manager

import (
	"crypto/sha1"
	"encoding/base64"
	"prikhodko.com/bee/hack/db"
	"prikhodko.com/bee/hack/common"
)

type (
	manager struct {
		db db.DB
	}

	ContractManager interface {
		GetContract(login string) (*common.Contract, error)
		SendUsdMoney(contract *common.Contract, count float64) error
		GetWalletAddress(contract *common.Contract, currency string) (string, error)
	}
)

func NewContractManager(db db.DB) ContractManager {
	return &manager{db: db}
}

func (this *manager) SendUsdMoney(contract *common.Contract, count float64) error {
	return nil
}

func (this *manager) GetContract(login string) (*common.Contract, error) {
	contract, err := this.db.GetContract(login)
	if err != nil {
		return nil, err
	}
	return contract, nil

}

//get wallet by contract + currency. return wallet address
func (this *manager) GetWalletAddress(contract *common.Contract, currency string) (string, error) {
	//TODO: azat's work
	hasher := sha1.New()
	hasher.Write([]byte(contract.Address + currency))
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	return sha, nil
}
