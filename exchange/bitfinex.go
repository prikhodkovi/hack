package exchange

import (
	"github.com/bitfinexcom/bitfinex-api-go/v1"
	"fmt"
	"strconv"
)

type bitfinexExchange struct {
	Exchange *bitfinex.Client
}

func NewBitfinexExchange() Exchange {
	return &bitfinexExchange{Exchange: bitfinex.NewClient().Auth("asd", "asd")}
}

func (this *bitfinexExchange) GetTicker(base, market string) (float64, float64, error) {
	if base == "USDT" {
		base = "USD"
	}
	if market == "USDT" {
		market = "USD"
	}
	ticker, err := this.Exchange.Ticker.Get(fmt.Sprintf("%s%s", market, base))
	if err != nil {
		return 0.0, 0.0, err
	}
	bid, _ := strconv.ParseFloat(ticker.Bid, 64)
	ask, _ := strconv.ParseFloat(ticker.Ask, 64)
	return bid, ask, nil
}

func (this *bitfinexExchange) Type()string{
	return "bitfinex"
}
