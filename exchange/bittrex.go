package exchange

import (
	"github.com/toorop/go-bittrex"
	"fmt"
)

type bittrexExchange struct {
	Exchange *bittrex.Bittrex
}

func NewBittrexExchange() Exchange {
	return &bittrexExchange{Exchange: bittrex.New("omg", "omg")}
}

func (this *bittrexExchange) GetTicker(base, market string) (float64, float64, error) {
	ticker, err := this.Exchange.GetTicker(fmt.Sprintf("%s-%s", base, market))
	if err != nil {
		return 0.0, 0.0, err
	}
	bid, _ := ticker.Bid.Float64()
	ask, _ := ticker.Ask.Float64()
	return bid, ask, nil
}

func (this *bittrexExchange) Type()string{
	return "bittrex"
}