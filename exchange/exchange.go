package exchange

type(
	Exchange interface{
		//bid and ask
		GetTicker(base, market string)(float64, float64, error)
		Type()string
	}
)
