package exchange

import (
	"github.com/jyap808/go-poloniex"
	"fmt"
	"errors"
)

//poloniex := poloniex.New(API_KEY, API_SECRET)

type poloniexExchange struct {
	Exchange *poloniex.Poloniex
}

func NewPoloniexExchange() Exchange {
	return &poloniexExchange{Exchange: poloniex.New("omg", "omg")}
}

func (this *poloniexExchange) GetTicker(base, market string) (float64, float64, error) {
	ticker, err := this.Exchange.GetTickers()
	if err != nil {
		return 0.0, 0.0, err
	}

	market_format := fmt.Sprintf("%s_%s", base, market)
	for k, v := range ticker {
		if k == market_format {
			bid, _ := v.Last.Float64()
			ask, _ := v.Last.Float64()
			return bid, ask, nil
		}
	}

	return 0.0, 0.0, errors.New("poloniex cannot support market " + market_format)
}

func (this *poloniexExchange) Type()string{
	return "poloniex"
}