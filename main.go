package main

import (
	"os"
	"flag"
	"prikhodko.com/bee/hack/contract_manager"
	"prikhodko.com/bee/hack/db"
	"prikhodko.com/bee/hack/logger"
	"prikhodko.com/bee/hack/srv"
	"prikhodko.com/bee/hack/money_manager"
)

func main() {
	flag.Parse()

	db, err := db.NewDb("root", "185.92.220.241", "ChangeMe")
	if err != nil {
		logger.Info("Cannot connect to db  %s", err.Error())
		os.Exit(1)
	}
	contract_mgr := contract_manager.NewContractManager(db)
	money_manger := money_manager.NewMoneyManager(db, contract_mgr)
	rsrv := srv.NewServer("8079", db, money_manger)
	rsrv.Start()

	wait()
}

func wait() {
	infchan := make(chan int)
	for {
		<-infchan
	}
}
