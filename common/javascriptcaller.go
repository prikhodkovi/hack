package common

import (
	"net/http"
	"io/ioutil"
	"b.yadro.com/storlib/logger"
	"time"
	"errors"
	"bytes"
	"encoding/json"
)

const (
	createPortfolio     = "http://localhost:3030/balances"
	checkPortfolio      = "http://localhost:3030/balances"
	addMoneyToPortfolio = "http://localhost:3030/balances"
)

func AddMoneyPortfolio(address string, amount float64) error {
	client := http.DefaultClient
	j := struct {
		Address string  `json:"address"`
		Amount  float64 `json:"amount"`
	}{Address: address, Amount: amount}
	jsonStr, _ := json.Marshal(j)
	req, err := http.NewRequest("POST", addMoneyToPortfolio, bytes.NewBuffer(jsonStr))
	if err != nil {
		return err
	}

	_, err = client.Do(req)
	return err
}

func CreateContract() (*Contract, error) {
	client := http.DefaultClient

	req, err := http.NewRequest("POST", createPortfolio, nil)
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	token := string(b)
	logger.Info("Got portfolio token %s ", token)

	ch := make(chan string)
	go func(c chan string) {
		tick := time.Tick(1 * time.Second)
		for {
			select {
			case <-tick:
				req, err := http.NewRequest("GET", checkPortfolio, nil)
				if err != nil {
					logger.Error("Canont create request %s", err.Error())
					continue
				}
				resp, err := client.Do(req)
				if err != nil {
					logger.Error("Canont perform request %s", err.Error())
					continue
				}
				defer resp.Body.Close()

				b, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					logger.Error("Cannot parse body %s", err.Error())
					continue
				}
				if string(b) == "OK" {
					return
				}
			}
		}
	}(ch)
	select {
	case ads := <-ch:
		logger.Info("finish task")
		return &Contract{Address: ads}, nil
	}
	logger.Info("finish")
	return nil, errors.New("test")
}
