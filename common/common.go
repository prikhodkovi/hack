package common

type UserInfo struct {
}

type Contract struct {
	Address string
}

type Action string

const (
	ACTION_BUY  Action = "BUY"
	ACTION_SELL Action = "SELL"
)

func Better(target, new_bid, new_ask float64, action Action) (bool, float64) {
	if action == ACTION_BUY {
		return new_ask <= target, new_ask
	} else {
		return new_bid >= target, new_bid
	}
}
