package srv

import (
	"net/http"
	"strconv"
	"prikhodko.com/bee/hack/contract_manager"
	"prikhodko.com/bee/hack/db"
	"prikhodko.com/bee/hack/logger"
	"prikhodko.com/bee/hack/money_manager"
	"prikhodko.com/bee/hack/common"
)

type (
	Server interface {
		Start()
		GetDb() db.DB
		GetMoneyMgr() money_manager.Money
	}

	//Server defines REST server interface
	server struct {
		port      string
		db        db.DB
		ctr_mgr   contract_manager.ContractManager
		money_mgr money_manager.Money
	}
)

func (this *server) GetMoneyMgr() money_manager.Money {
	return this.money_mgr
}

func (this *server) GetDb() db.DB {
	return this.db
}

const (
	user   = "/user"
	money  = "/money"
	change = "/change"
)

//NewServer is a constructor method for Server instance
func NewServer(port string, db db.DB, money_mgr money_manager.Money) Server {
	return &server{port: port, db: db, money_mgr: money_mgr}
}

func getHandler(s Server, f func(w http.ResponseWriter, r *http.Request, s Server, args ...interface{}), args ...interface{}) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		f(w, r, s, args...)
	})
}

func testHandler(w http.ResponseWriter, r *http.Request, s Server, args ...interface{}) {
	w.Write([]byte("strng"))
}

func userHandler(w http.ResponseWriter, r *http.Request, s Server, _ ...interface{}) {
	login := r.Header.Get("login")
	password := r.Header.Get("password")

	contract, err := common.CreateContract()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	logger.Info("Add user = %s pass = %s", login, password)
	err = s.GetDb().AddUser(login, password)
	if err != nil {
		logger.Error("Cannot create user err = %s", err.Error())
		return
	}

	err = s.GetDb().AddContract(login, contract)
	if err != nil {
		logger.Error("Cannot create user err = %s", err.Error())
		return
	}
	w.Write([]byte("user"))
}

func changeHandler(w http.ResponseWriter, r *http.Request, s Server, _ ...interface{}) {
	login := r.Header.Get("login")
	password := r.Header.Get("password")
	base := r.Header.Get("base")
	market := r.Header.Get("market")
	amount, _ := strconv.Atoi(r.Header.Get("amount"))
	action := r.Header.Get("action")
	_, err := s.GetDb().GetUserInfo(login, password)
	if err != nil {
		logger.Error("Error %s", err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	err = s.GetMoneyMgr().Change(login, base, market, uint32(amount), common.Action(action))
	if err != nil {
		logger.Error("Error %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func moneyHandler(w http.ResponseWriter, r *http.Request, s Server, _ ...interface{}) {
	login := r.Header.Get("login")
	password := r.Header.Get("password")
	usd, _ := strconv.ParseFloat(r.Header.Get("usd"), 64)
	_, err := s.GetDb().GetUserInfo(login, password)
	if err != nil {
		logger.Error("Error %s", err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	ctr, err := s.GetDb().GetContract(login)
	if err != nil {
		logger.Error("Cannot find portfolio for user %s. %s", login, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = common.AddMoneyPortfolio(ctr.Address, usd)
	if err != nil {
		logger.Error("Cannot add money to portfolio %s. %s", login, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = s.GetMoneyMgr().TakeMyMoney(login, password, usd)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write([]byte("money"))
}

func (this *server) Start() {
	logger.Info("Starting server + " + this.port)
	http.Handle("/test", getHandler(this, testHandler))
	http.Handle(user, getHandler(this, userHandler))
	http.Handle(money, getHandler(this, moneyHandler))
	http.Handle(change, getHandler(this, changeHandler))

	go func() {
		http.ListenAndServe(":"+this.port, nil)
	}()
}
