package db

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"fmt"
	"prikhodko.com/bee/hack/common"
	"prikhodko.com/bee/hack/logger"
	"github.com/satori/go.uuid"
)

type (
	mydb struct {
		db *sql.DB
	}

	DB interface {
		AddUser(login, pass string) error
		GetUserInfo(login, pass string) (*common.UserInfo, error)

		AddContract(login string, contract *common.Contract) error
		GetContract(login string) (*common.Contract, error)

		GetSecret(login string, contract *common.Contract, currency string) (string, error)
	}
)

func NewDb(user, ip, password string) (DB, error) {
	_db, err := sql.Open("mysql",
		user+":"+password+"@tcp("+ip+":3306)/hack")

	if err != nil {
		return nil, err
	}
	err = _db.Ping()
	if err != nil {
		return nil, err
	}

	return &mydb{db: _db}, err
}

func (this *mydb) AddUser(login, pass string) error {
	_, err := this.db.Exec("INSERT INTO `users` (`user`,`pass`) VALUES (?,?)",
		login, pass)
	return err
}

func (this *mydb) GetUserInfo(login, pass string) (*common.UserInfo, error) {
	var count int
	err := this.db.QueryRow("select COUNT(*) from users where user = ? AND pass = ?", login, pass).Scan(&count)
	if err != nil {
		return nil, err
	}
	if count != 1 {
		return nil, fmt.Errorf("cannot find user %s with given password", login)
	}
	return &common.UserInfo{}, nil
}

func (this *mydb) AddContract(login string, contract *common.Contract) error {
	var user_id int
	err := this.db.QueryRow("select id from users where user = ?", login).Scan(&user_id)
	if err != nil {
		return err
	}

	logger.Info("Create contract for %s (%d) ", login, user_id)
	_, err = this.db.Exec("INSERT INTO `contracts` (`user_id`,`address`) VALUES (?,?)",
		user_id, contract.Address)
	return err
}

func (this *mydb) GetContract(login string) (*common.Contract, error) {
	var user_id int
	err := this.db.QueryRow("select id from users where user = ?", login).Scan(&user_id)
	if err != nil {
		return nil, err
	}

	var addr string
	err = this.db.QueryRow("select address from contracts where user_id = ?", user_id).Scan(&addr)
	if err != nil {
		return nil, err
	}

	return &common.Contract{Address: addr}, nil
}

func (this *mydb) GetSecret(login string, contract *common.Contract, currency string) (string, error) {
	var user_id int
	err := this.db.QueryRow("select id from users where user = ?", login).Scan(&user_id)
	if err != nil {
		return "", err
	}

	var secret string
	err = this.db.QueryRow("select secret from wallets where user_id = ? and address= ? and currency=?",
		user_id, contract.Address, currency).Scan(&secret)
	if err != nil {
		//generate secret
		logger.Info("Generate secret")
		secret = uuid.NewV4().String()
		_, err = this.db.Exec("INSERT INTO `wallets` (`user_id`,`address`, `secret`, `currency`) VALUES (?,?,?,?)",
			user_id, contract.Address, secret, currency)
		return secret, err
	}

	return secret, nil
}
